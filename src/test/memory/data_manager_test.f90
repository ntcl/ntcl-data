module data_manager_test_module
    use :: iso_fortran_env, only : int64

    use :: util_api, only : &
            assert, &
            string

    use :: util_api, only : &
            tile

    use :: data_api, only : &
            data_storage, &
            memory_factory, &
            memory_manager, &
            storage_helper, &
            allocate_and_copy_storage

    implicit none
    private

    public :: data_manager_test

    type :: data_manager_test
    contains
        procedure :: run => run
        procedure :: run_test => run_test
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type data_manager_test

    interface data_manager_test
        module procedure constructor
    end interface data_manager_test
contains
    function constructor() result(this)
        type(data_manager_test) :: this

        call this%clear()
    end function constructor

    subroutine run(this, assertion)
        class(data_manager_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(string), dimension(:), allocatable :: memory_types
        integer :: idx, midx
        type(string) :: prefix, my_prefix

        call assertion%equal("data_manager::Test complete", .true.)

        memory_types = memory_factory%get_available_memory_types()

        prefix = "data_manager::"
        do idx = 1, size(memory_types)
            do midx = 1, size(memory_types)
                my_prefix = prefix%char_array//"src_memory_type="//memory_types(midx)%char_array//","// &
                        "dst_memory_type="//memory_types(idx)%char_array//":"
                call this%run_test(assertion, my_prefix%char_array, &
                        memory_types(idx)%char_array, memory_types(midx)%char_array)
            end do
        end do
    end subroutine run

    subroutine run_test(this, assertion, prefix, dst_memory_type, src_memory_type)
        class(data_manager_test), intent(in) :: this
        type(assert), intent(inout) :: assertion
        character(len=*), intent(in) :: prefix
        character(len=*), intent(in) ::  dst_memory_type, src_memory_type

        real, dimension(11,33) :: fdst
        real, dimension(9,27) :: fsrc
        class(data_storage), allocatable :: src, dst
        type(tile) :: stile, dtile, src_tile, dst_tile
        integer(int64), dimension(2) :: src_dims, dst_dims
        integer :: nbytes
        type(storage_helper) :: helper
        integer :: scol, dcol, srow, drow, row, col

        nbytes = 4

        call random_number(fsrc)
        call random_number(fdst)

        call allocate_and_copy_storage(dst, fdst, dst_memory_type)
        call allocate_and_copy_storage(src, fsrc, src_memory_type)

        stile = tile(2,6,5,18)
        dtile = tile(7,11,8,21)
        do col = 1, stile%col_size
            scol = stile%col_offset + col
            dcol = dtile%col_offset + col
            do row = 1, stile%row_size
                srow = stile%row_offset + row
                drow = dtile%row_offset + row
                fdst(drow, dcol) = fsrc(srow, scol)
            end do
        end do

        src_dims = [size(fsrc, 1)*nbytes, size(fsrc, 2)]
        src_tile = tile(stile%row_offset*nbytes+1, stile%row_end*nbytes, stile%col_start, stile%col_end)
        dst_dims = [size(fdst, 1)*nbytes, size(fdst, 2)]
        dst_tile = tile(dtile%row_offset*nbytes+1, dtile%row_end*nbytes, dtile%col_start, dtile%col_end)

        call memory_manager%copy_2d(dst, dst_dims, dst_tile, src, src_dims, src_tile)
        call assertion%equal(prefix//"copy_2d", helper%equal(dst, fdst, 1.0e-5))

        call dst%deallocate_data()
        call src%deallocate_data()
    end subroutine run_test

    subroutine cleanup(this)
        class(data_manager_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(data_manager_test), intent(inout) :: this
    end subroutine clear
end module data_manager_test_module
