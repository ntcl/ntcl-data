module fhc_with_buffer_test_module
    use :: util_api, only : &
            assert, &
            string, &
            dictionary

    use :: data_api, only : &
            fortran_converter_factory, &
            fortran_host_converter

    use :: data_dev, only : &
            fhc_with_buffer

    implicit none
    private

    public :: fhc_with_buffer_test

    type :: fhc_with_buffer_test
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type fhc_with_buffer_test

    interface fhc_with_buffer_test
        module procedure constructor
    end interface fhc_with_buffer_test
contains
    function constructor() result(this)
        type(fhc_with_buffer_test) :: this

        call this%clear()
    end function constructor

    subroutine run(this, assertion)
        class(fhc_with_buffer_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        class(fortran_host_converter), allocatable :: fhc
        type(fhc_with_buffer) :: correct_type
        type(fortran_converter_factory) :: factory
        type(dictionary) :: options

        call assertion%equal("fhc_with_buffer::Test complete", .true.)

        options = dictionary()
        call options%set_value("converter_driver", "buffered")
        call options%set_value("converter-memory_type", "host")
        call options%set_value("converter-scratch_buffer-size", "1000")
        call options%set_value("converter-scratch_buffer-number_of_checkpoints", "3")
        call options%set_value("converter-scratch_buffer-number_of_slots", "10")

        call factory%build_host_converter(fhc, options=options)

        call assertion%equal("fhc_with_buffer::Allocated", &
                allocated(fhc))
        call assertion%equal("fhc_with_buffer::Correct type", &
                same_type_as(correct_type, fhc))
        select type(fhc)
        type is (fhc_with_buffer)
            call assertion%equal("fhc_with_buffer::Correct buffer size", &
                fhc%buffer%buffer%get_available_size() == 1000)
            call assertion%equal("fhc_with_buffer::Correct number of checkpoints", &
                fhc%buffer%checkpoints%counter%array_size == 3)
            call assertion%equal("fhc_with_buffer::Correct number of slots", &
                fhc%buffer%slots%counter%array_size == 10)
        end select

        call fhc%cleanup()

    end subroutine run

    subroutine cleanup(this)
        class(fhc_with_buffer_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(fhc_with_buffer_test), intent(inout) :: this
    end subroutine clear
end module fhc_with_buffer_test_module
