! Auto-generated -- DO NOT MODIFY
module cuda_plugin_package_test_module
    use :: util_api, only : &
            selector, &
            assert

    use :: event_pool_cuda_test_module, only : event_pool_cuda_test
    use :: stream_pool_cuda_test_module, only : stream_pool_cuda_test

    implicit none
    private

    public :: cuda_plugin_package_test

    type :: cuda_plugin_package_test
        type(selector) :: test_selector
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type cuda_plugin_package_test

    interface cuda_plugin_package_test
        module procedure constructor
    end interface cuda_plugin_package_test

contains
    function constructor(aselector) result(this)
        type(selector), intent(in) :: aselector
        type(cuda_plugin_package_test) :: this

        call this%clear()

        this%test_selector = aselector
    end function constructor

    subroutine run(this, assertion)
        class(cuda_plugin_package_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(event_pool_cuda_test) :: aevent_pool_cuda_test
        type(stream_pool_cuda_test) :: astream_pool_cuda_test

        call assertion%equal("cuda_plugin::Package test complete", .true.)

        if ( &
                this%test_selector%is_enabled("event_pool_cuda") ) then
            aevent_pool_cuda_test = event_pool_cuda_test()
            call aevent_pool_cuda_test%run(assertion)
            call aevent_pool_cuda_test%cleanup()
        end if

        if ( &
                this%test_selector%is_enabled("stream_pool_cuda") ) then
            astream_pool_cuda_test = stream_pool_cuda_test()
            call astream_pool_cuda_test%run(assertion)
            call astream_pool_cuda_test%cleanup()
        end if

    end subroutine run

    subroutine cleanup(this)
        class(cuda_plugin_package_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(cuda_plugin_package_test), intent(inout) :: this
    end subroutine clear
end module cuda_plugin_package_test_module
