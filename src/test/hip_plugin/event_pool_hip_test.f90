module event_pool_hip_test_module
    use :: iso_c_binding, only : &
            c_associated

    use :: util_api, only : assert

    use :: data_api, only : &
            event, &
            event_pool

    use :: hip_concurrency_interface, only : &
            hip_query_event

    implicit none
    private

    public :: event_pool_hip_test

    type :: event_pool_hip_test
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type event_pool_hip_test

    interface event_pool_hip_test
        module procedure constructor
    end interface event_pool_hip_test
contains
    function constructor() result(this)
        type(event_pool_hip_test) :: this

        call this%clear()
    end function constructor

    subroutine run(this, assertion)
        class(event_pool_hip_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(event_pool) :: aevent_pool
        type(event) :: an_event

        call assertion%equal("event_pool_hip::Test complete", .true.)

        aevent_pool = event_pool()
        an_event = aevent_pool%get_next_event()
        call assertion%equal("event_pool_hip::event_pool():get_next_event():Not associated", &
                .not. c_associated(an_event%eid) )

        an_event = aevent_pool%get_next_available_event()
        call assertion%equal("event_pool_hip::event_pool():get_next_available_event():Not associated", &
                .not. c_associated(an_event%eid) )

        aevent_pool = event_pool(10)
        call assertion%equal("event_pool_hip::event_pool(10):counter size", &
                aevent_pool%counter%counter_size == 10)
        call assertion%equal("event_pool_hip::event_pool(10):current counter", &
                aevent_pool%counter%get_next() == 1)
        call assertion%equal("event_pool_hip::event_pool(10):Array allocated", &
                allocated(aevent_pool%events) )
        if ( allocated(aevent_pool%events) ) then
            call assertion%equal("event_pool_hip::event_pool(10):Array size", &
                    size(aevent_pool%events) == 10)
        end if

        an_event = aevent_pool%get_next_event()
        call assertion%equal("event_pool_hip::event_pool(10):get_next_event():Associated", &
                c_associated(an_event%eid) )
        if ( c_associated(an_event%eid) ) then
            call assertion%equal("event_pool_hip::event_pool(10):get_next_event():hipEventQuery", &
                    hip_query_event(an_event%eid) == 0)
        end if
        an_event = aevent_pool%get_next_available_event()
        call assertion%equal("event_pool_hip::event_pool(10):get_next_available_event():Associated", &
                c_associated(an_event%eid) )
        if ( c_associated(an_event%eid) ) then
            call assertion%equal("event_pool_hip::event_pool(10):get_next_available_event():hipEventQuery", &
                    hip_query_event(an_event%eid) == 0)
        end if

        call aevent_pool%cleanup()
        call assertion%equal("event_pool_hip::After cleanup:counter size", &
                aevent_pool%counter%counter_size == 0)
        call assertion%equal("event_pool_hip::After cleanup:current counter", &
                aevent_pool%counter%get_next() == 1)
        call assertion%equal("event_pool_hip::After cleanup:Array not allocated", &
                .not. allocated(aevent_pool%events) )

    end subroutine run

    subroutine cleanup(this)
        class(event_pool_hip_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(event_pool_hip_test), intent(inout) :: this
    end subroutine clear
end module event_pool_hip_test_module
