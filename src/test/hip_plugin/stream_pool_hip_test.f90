module stream_pool_hip_test_module
    use :: iso_c_binding, only : &
            c_associated

    use :: util_api, only : assert

    use :: data_api, only : &
            stream, &
            stream_pool

    use :: hip_concurrency_interface, only : &
            hip_query_stream

    implicit none
    private

    public :: stream_pool_hip_test

    type :: stream_pool_hip_test
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type stream_pool_hip_test

    interface stream_pool_hip_test
        module procedure constructor
    end interface stream_pool_hip_test
contains
    function constructor() result(this)
        type(stream_pool_hip_test) :: this

        call this%clear()
    end function constructor

    subroutine run(this, assertion)
        class(stream_pool_hip_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(stream_pool) :: astream_pool
        type(stream) :: astream

        call assertion%equal("stream_pool_hip::Test complete", .true.)

        astream_pool = stream_pool()
        astream = astream_pool%get_next_stream()
        call assertion%equal("stream_pool_hip::stream_pool():get_next_stream():Not associated", &
                .not. c_associated(astream%sid) )

        astream_pool = stream_pool(10)
        call assertion%equal("stream_pool_hip::stream_pool(10):counter size", &
                astream_pool%counter%counter_size == 10)
        call assertion%equal("stream_pool_hip::stream_pool(10):current counter", &
                astream_pool%counter%get_next() == 1)
        call assertion%equal("stream_pool_hip::stream_pool(10):Array allocated", &
                allocated(astream_pool%streams) )
        if ( allocated(astream_pool%streams) ) then
            call assertion%equal("stream_pool_hip::stream_pool(10):Array size", &
                    size(astream_pool%streams) == 10)
        end if

        astream = astream_pool%get_next_stream()
        call assertion%equal("stream_pool_hip::stream_pool(10):get_next_stream():Associated", &
                c_associated(astream%sid) )
        if ( c_associated(astream%sid) ) then
            call assertion%equal("stream_pool_hip::stream_pool(10):get_next_stream():hipEventQuery", &
                    hip_query_stream(astream%sid) == 0)
        end if

        call astream_pool%cleanup()
        call assertion%equal("stream_pool_hip::After cleanup:counter size", &
                astream_pool%counter%counter_size == 0)
        call assertion%equal("stream_pool_hip::After cleanup:current counter", &
                astream_pool%counter%get_next() == 1)
        call assertion%equal("stream_pool_hip::After cleanup:Array not allocated", &
                .not. allocated(astream_pool%streams) )

    end subroutine run

    subroutine cleanup(this)
        class(stream_pool_hip_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(stream_pool_hip_test), intent(inout) :: this
    end subroutine clear
end module stream_pool_hip_test_module
