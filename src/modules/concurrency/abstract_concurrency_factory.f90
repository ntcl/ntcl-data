module abstract_concurrency_factory_module
    use :: util_api, only : &
            string, &
            dictionary, &
            dictionary_converter

    use :: stream_module, only : stream
    use :: stream_handler_module, only : stream_handler
    use :: event_module, only : event
    use :: event_handler_module, only : event_handler
    use :: abstract_dependency_manager_module, only : abstract_dependency_manager

    implicit none
    private

    public :: abstract_concurrency_factory

    type, abstract :: abstract_concurrency_factory
    contains
        procedure :: get_dependency_manager => get_dependency_manager
        procedure :: get_stream => get_stream
        procedure :: create_stream => create_stream
        procedure :: get_event => get_event
        procedure :: create_event => create_event
        procedure(create_dependency_manager_interface), deferred :: create_dependency_manager
        procedure(create_stream_handler_interface), deferred :: create_stream_handler
        procedure(create_event_handler_interface), deferred :: create_event_handler
        procedure(empty), deferred :: cleanup
        procedure :: cleanup_abstract_concurrency_factory => cleanup_abstract_concurrency_factory
    end type abstract_concurrency_factory

    abstract interface
        subroutine empty(this)
            import :: abstract_concurrency_factory

            class(abstract_concurrency_factory), intent(inout) :: this
        end subroutine empty

        subroutine create_dependency_manager_interface(this, manager, config, priorities)
            import :: abstract_concurrency_factory
            import :: abstract_dependency_manager
            import :: dictionary
            import ::string

            class(abstract_concurrency_factory), intent(in) :: this
            class(abstract_dependency_manager), intent(inout), allocatable :: manager
            type(dictionary), intent(in), optional :: config
            type(string), dimension(:), intent(in), optional :: priorities
        end subroutine create_dependency_manager_interface

        subroutine create_stream_handler_interface(this, handler)
            import :: abstract_concurrency_factory
            import :: stream_handler

            class(abstract_concurrency_factory), intent(in) :: this
            class(stream_handler), allocatable, intent(inout) :: handler
        end subroutine create_stream_handler_interface

        subroutine create_event_handler_interface(this, handler)
            import :: abstract_concurrency_factory
            import :: event_handler

            class(abstract_concurrency_factory), intent(in) :: this
            class(event_handler), allocatable, intent(inout) :: handler
        end subroutine create_event_handler_interface
    end interface
contains
    function get_dependency_manager(this, config, priorities) result(manager)
        class(abstract_concurrency_factory), intent(in) :: this
        type(dictionary), intent(in), optional :: config
        type(string), dimension(:), intent(in), optional :: priorities
        class(abstract_dependency_manager), allocatable :: manager

        call this%create_dependency_manager(manager, config, priorities)
    end function get_dependency_manager

    function get_stream(this) result(astream)
        class(abstract_concurrency_factory), intent(in) :: this
        type(stream) :: astream

        call this%create_stream(astream)
    end function get_stream

    subroutine create_stream(this, astream)
        class(abstract_concurrency_factory), intent(in) :: this
        type(stream), intent(inout) :: astream

        class(stream_handler), allocatable :: handler

        call this%create_stream_handler(handler)
        call handler%create(astream)
        deallocate(handler)
    end subroutine create_stream

    function get_event(this) result(anevent)
        class(abstract_concurrency_factory), intent(in) :: this
        type(event) :: anevent

        call this%create_event(anevent)
    end function get_event

    subroutine create_event(this, anevent)
        class(abstract_concurrency_factory), intent(in) :: this
        type(event), intent(inout) :: anevent

        class(event_handler), allocatable :: handler

        call this%create_event_handler(handler)
        call handler%create(anevent)
        deallocate(handler)
    end subroutine create_event

    subroutine cleanup_abstract_concurrency_factory(this)
        class(abstract_concurrency_factory), intent(inout) :: this

    end subroutine cleanup_abstract_concurrency_factory
end module abstract_concurrency_factory_module
