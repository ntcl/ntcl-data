module cuda_data_wrapper_module
    use, intrinsic :: iso_c_binding, only : &
        c_ptr, &
        c_size_t, &
        c_int

    implicit none
    private

    public :: cuda_update_device
    public :: cuda_update_host
    public :: cuda_update_device_async
    public :: cuda_update_device_async_with_stream
    public :: cuda_update_host_async
    public :: cuda_update_host_async_with_stream
    public :: cuda_device2device
    public :: cuda_device2device_with_stream
    public :: cuda_device2device_2d
    public :: cuda_host2device_2d
    public :: cuda_device2host_2d

    integer, public, parameter :: CUDA_DATA_SUCCESS = 0

    interface
        integer(c_int) function cuda_update_device( dst, src, nbytes) &
                                                bind(c, name="cuda_update_device")
            import :: c_ptr, c_size_t, c_int
            type(c_ptr), value :: dst, src
            integer(c_size_t), value :: nbytes
        end function cuda_update_device

        integer(c_int) function cuda_update_host( dst, src, nbytes) &
                                                bind(c, name="cuda_update_host")
            import :: c_ptr, c_size_t, c_int
            type(c_ptr), value :: dst, src
            integer(c_size_t), value :: nbytes
        end function cuda_update_host

        integer(c_int) function cuda_update_device_async( dst, src, nbytes) &
                                                bind(c, name="cuda_update_device_async")
            import :: c_ptr, c_size_t, c_int
            type(c_ptr), value :: dst, src
            integer(c_size_t), value :: nbytes
        end function cuda_update_device_async

        integer(c_int) function cuda_update_host_async( dst, src, nbytes) &
                                                bind(c, name="cuda_update_host_async")
            import :: c_ptr, c_size_t, c_int
            type(c_ptr), value :: dst, src
            integer(c_size_t), value :: nbytes
        end function cuda_update_host_async

        integer(c_int) function cuda_update_device_async_with_stream( dst, src, nbytes, astream) &
                                                bind(c, name="cuda_update_device_async_with_stream")
            import :: c_ptr, c_size_t, c_int
            type(c_ptr), value :: dst, src
            integer(c_size_t), value :: nbytes
            type(c_ptr), value :: astream
        end function cuda_update_device_async_with_stream

        integer(c_int) function cuda_update_host_async_with_stream( dst, src, nbytes, astream) &
                                                bind(c, name="cuda_update_host_async_with_stream")
            import :: c_ptr, c_size_t, c_int
            type(c_ptr), value :: dst, src
            integer(c_size_t), value :: nbytes
            type(c_ptr), value :: astream
        end function cuda_update_host_async_with_stream

        integer(c_int) function cuda_device2device( dst, src, nbytes) &
                                                bind(c, name="cuda_device2device")
            import :: c_ptr, c_size_t, c_int
            type(c_ptr), value :: dst, src
            integer(c_size_t), value :: nbytes
        end function cuda_device2device

        integer(c_int) function cuda_device2device_with_stream( dst, src, nbytes, astream) &
                                                bind(c, name="cuda_device2device_with_stream")
            import :: c_ptr, c_size_t, c_int
            type(c_ptr), value :: dst, src
            integer(c_size_t), value :: nbytes
            type(c_ptr), value :: astream
        end function cuda_device2device_with_stream

        integer(c_int) function cuda_device2device_2d(dst, dst_offset, dst_pitch, &
                src, src_offset, src_pitch, width, height, astream) &
                                                bind(c, name="cuda_device2device_2d")
            import :: c_ptr, c_size_t, c_int

            type(c_ptr), value :: dst, src
            integer(c_size_t), value :: dst_offset, dst_pitch, src_offset, src_pitch, width, height
            type(c_ptr), value :: astream
        end function cuda_device2device_2d

        integer(c_int) function cuda_host2device_2d(dst, dst_offset, dst_pitch, &
                src, src_offset, src_pitch, width, height, astream) &
                                                bind(c, name="cuda_host2device_2d")
            import :: c_ptr, c_size_t, c_int

            type(c_ptr), value :: dst, src
            integer(c_size_t), value :: dst_offset, dst_pitch, src_offset, src_pitch, width, height
            type(c_ptr), value :: astream
        end function cuda_host2device_2d

        integer(c_int) function cuda_device2host_2d(dst, dst_offset, dst_pitch, &
                src, src_offset, src_pitch, width, height, astream) &
                                                bind(c, name="cuda_device2host_2d")
            import :: c_ptr, c_size_t, c_int

            type(c_ptr), value :: dst, src
            integer(c_size_t), value :: dst_offset, dst_pitch, src_offset, src_pitch, width, height
            type(c_ptr), value :: astream
        end function cuda_device2host_2d
    end interface
end module cuda_data_wrapper_module

