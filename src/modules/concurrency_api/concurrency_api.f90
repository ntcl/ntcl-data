module concurrency_api
    use :: util_api, only : &
            string, &
            dictionary, &
            dictionary_converter

    use :: event_module, only : event
    use :: event_handler_module, only : event_handler
    use :: stream_module, only : stream
    use :: stream_handler_module, only : stream_handler
    use :: abstract_dependency_manager_module, only : abstract_dependency_manager
    use :: abstract_concurrency_factory_module, only : abstract_concurrency_factory

    implicit none
    private

    public :: event
    public :: event_handler
    public :: stream
    public :: stream_handler
    public :: abstract_dependency_manager
    public :: abstract_concurrency_factory
    public :: concurrency_factory
    public :: dependency_manager

    public :: concurrency_initialize
    public :: concurrency_finalize

    class(abstract_concurrency_factory), allocatable :: concurrency_factory
    class(abstract_dependency_manager), allocatable :: dependency_manager
contains
    subroutine concurrency_initialize(factory, options)
        class(abstract_concurrency_factory), intent(in) :: factory
        type(dictionary), intent(in), optional :: options

        if ( allocated(concurrency_factory) .or. allocated(dependency_manager) ) &
                error stop 'concurrency_initialize::Already initialized.'

        concurrency_factory = factory

        call concurrency_factory%create_dependency_manager( &
                dependency_manager, &
                options, &
                [string("global_dependency_manager-")])
    end subroutine concurrency_initialize

    subroutine concurrency_finalize()

        if ( allocated(concurrency_factory) ) then
            call concurrency_factory%cleanup()
            deallocate(concurrency_factory)
        end if

        if ( allocated(dependency_manager) ) then
            call dependency_manager%cleanup()
            deallocate(dependency_manager)
        end if
    end subroutine concurrency_finalize
end module concurrency_api
