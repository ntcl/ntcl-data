module hip_data_manager_module
    use, intrinsic :: iso_fortran_env, only : int64
    use, intrinsic :: iso_c_binding, only : c_int, c_size_t, c_null_ptr

    use :: util_api, only : tile

    use :: data_manager_module, only : data_manager
    use :: data_storage_module, only : data_storage
    use :: hip_data_wrapper_module, only : &
            hip_device2device, &
            hip_device2device_with_stream, &
            hip_data_success, &
            hip_device2device_2d
    use :: hip_storage_module, only : hip_storage
    use :: stream_module, only : stream

    implicit none
    private

    public :: hip_data_manager

    type, extends(data_manager) :: hip_data_manager
    contains
        procedure :: copy_storage => copy_storage
        procedure :: copy_2d => copy_2d
        procedure :: secure_local_storage => secure_local_storage
        procedure :: secure_local_storage_wo_copy => secure_local_storage
        procedure :: update_remote_storage => update_remote_storage
        procedure :: release_local_storage => release_local_storage
        procedure :: copy_device_data => copy_device_data
        procedure :: copy_device_data_2d => copy_device_data_2d
        procedure :: point_to_device_data => point_to_device_data
        procedure :: update_device_storage_from_device => update_device_storage_from_device
        procedure :: release_device_storage_from_device => release_device_storage_from_device
    end type hip_data_manager
contains
    subroutine copy_storage(this, src, dst, astream)
        class(hip_data_manager), intent(in) :: this
        class(data_storage), intent(in) :: src
        class(data_storage), intent(inout) :: dst
        type(stream), intent(in), optional :: astream

        select type (src)
        type is (hip_storage)
            select type (dst)
            type is (hip_storage)
                call this%copy_device_data(src, dst, astream)
            end select
        end select
    end subroutine copy_storage

    subroutine copy_2d(this, dst, dst_dims, dst_tile, src, src_dims, src_tile, astream)
        class(hip_data_manager), intent(in) :: this
        class(data_storage), intent(inout) :: dst
        integer(int64), dimension(2), intent(in) :: dst_dims
        type(tile), intent(in) :: dst_tile
        class(data_storage), intent(in) :: src
        integer(int64), dimension(2), intent(in) :: src_dims
        type(tile), intent(in) :: src_tile
        type(stream), intent(in), optional :: astream

        select type (src)
        type is (hip_storage)
            select type (dst)
            type is (hip_storage)
                call this%copy_device_data_2d(dst, dst_dims, dst_tile, src, src_dims, src_tile, astream)
            end select
        end select

    end subroutine copy_2d

    subroutine secure_local_storage(this, src, dst, astream)
        class(hip_data_manager), intent(in) :: this
        class(data_storage), intent(in) :: src
        class(data_storage), intent(inout) :: dst
        type(stream), intent(in), optional :: astream

        select type (src)
        type is (hip_storage)
            select type (dst)
            type is (hip_storage)
                call this%point_to_device_data(src, dst)
            end select
        end select
    end subroutine secure_local_storage

    subroutine update_remote_storage(this, src, dst, astream)
        class(hip_data_manager), intent(in) :: this
        class(data_storage), intent(in) :: src
        class(data_storage), intent(inout) :: dst
        type(stream), intent(in), optional :: astream

        select type (src)
        type is (hip_storage)
            select type (dst)
            type is (hip_storage)
                call this%update_device_storage_from_device(src, dst)
            end select
        end select
    end subroutine update_remote_storage

    subroutine release_local_storage(this, src, dst, astream)
        class(hip_data_manager), intent(in) :: this
        class(data_storage), intent(in) :: src
        class(data_storage), intent(inout) :: dst
        type(stream), intent(in), optional :: astream

        select type (src)
        type is (hip_storage)
            select type (dst)
            type is (hip_storage)
                call this%release_device_storage_from_device(src, dst)
            end select
        end select
    end subroutine release_local_storage

    subroutine copy_device_data(this, src, dst, astream)
        class(hip_data_manager), intent(in) :: this
        type(hip_storage), intent(in) :: src
        type(hip_storage), intent(inout) :: dst
        type(stream), intent(in), optional :: astream

        integer(c_int) :: error

        if ( src%is_allocated ) then
            call dst%allocate_data(src%number_of_bytes)
        else
            return
        end if

        if ( present( astream) ) then
            error = hip_device2device_with_stream(dst%get_data_pointer(), src%get_data_pointer(), &
                    int(src%number_of_bytes, c_size_t), astream%sid)
        else
            error = hip_device2device(dst%get_data_pointer(), src%get_data_pointer(), &
                    int(src%number_of_bytes, c_size_t))
        end if
        if ( error /= hip_data_success ) call dst%deallocate_data()
    end subroutine copy_device_data

    subroutine copy_device_data_2d(this, dst, dst_dims, dst_tile, src, src_dims, src_tile, astream)
        class(hip_data_manager), intent(in) :: this
        type(hip_storage), intent(inout) :: dst
        integer(int64), dimension(2), intent(in) :: dst_dims
        type(tile), intent(in) :: dst_tile
        type(hip_storage), intent(in) :: src
        integer(int64), dimension(2), intent(in) :: src_dims
        type(tile), intent(in) :: src_tile
        type(stream), intent(in), optional :: astream

        integer(c_size_t) :: src_offset, dst_offset, height, width, src_pitch, dst_pitch
        integer(c_int) :: error

        dst_pitch = dst_dims(1)
        dst_offset = dst_tile%col_offset*dst_pitch + dst_tile%row_offset

        src_pitch = src_dims(1)
        src_offset = src_tile%col_offset*src_pitch + src_tile%row_offset

        height = dst_tile%col_size
        width = dst_tile%row_size

        if ( present(astream) ) then
            error = hip_device2device_2d(dst%get_data_pointer(), dst_offset, dst_pitch, &
                    src%get_data_pointer(), src_offset, src_pitch, width, height, astream%sid)
        else
            error = hip_device2device_2d(dst%get_data_pointer(), dst_offset, dst_pitch, &
                    src%get_data_pointer(), src_offset, src_pitch, width, height, c_null_ptr)

        end if
        if ( error /= hip_data_success ) &
                error stop "hip_data_manager::copy_device_data_2d:Could not copy data."
    end subroutine copy_device_data_2d

    subroutine point_to_device_data(this, src, dst)
        class(hip_data_manager), intent(in) :: this
        type(hip_storage), intent(in) :: src
        type(hip_storage), intent(inout) :: dst

        dst = hip_storage(src%get_data_pointer(), src%number_of_bytes)
    end subroutine point_to_device_data

    subroutine update_device_storage_from_device(this, src, dst)
        class(hip_data_manager), intent(in) :: this
        type(hip_storage), intent(in) :: src
        type(hip_storage), intent(inout) :: dst

        ! Do nothing.
        ! Maybe check for pointer equivalence
    end subroutine update_device_storage_from_device

    subroutine release_device_storage_from_device(this, src, dst)
        class(hip_data_manager), intent(in) :: this
        type(hip_storage), intent(in) :: src
        type(hip_storage), intent(inout) :: dst

        ! Do nothing.
        ! Maybe check for pointer equivalence
    end subroutine release_device_storage_from_device

end module hip_data_manager_module
