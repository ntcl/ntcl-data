module fortran_converter_factory_module
    use :: util_api, only : &
            string, &
            dictionary, &
            dictionary_converter, &
            add_prefix_to_priorities

    use :: memory_api, only : &
            memory_factory, &
            data_builder

    use :: host_memory_plugin, only : host_builder

    use :: scratch_buffer_builder_module, only : scratch_buffer_builder

    use :: fortran_data_converter_module, only : fortran_data_converter
    use :: fortran_host_converter_module, only : fortran_host_converter
    use :: fhc_with_buffer_module, only : fhc_with_buffer

    implicit none
    private

    public :: fortran_converter_factory

    type :: fortran_converter_factory
    contains
        generic :: get_data_converter => &
                get_data_converter_from_string, &
                get_data_converter_from_chars
        generic :: build_data_converter => &
                build_data_converter_from_string, &
                build_data_converter_from_chars
        generic :: get_host_converter => &
                get_host_converter_from_string, &
                get_host_converter_from_chars
        generic :: build_host_converter => &
                build_host_converter_from_string, &
                build_host_converter_from_chars
        procedure :: get_data_converter_from_string => get_data_converter_from_string
        procedure :: get_data_converter_from_chars => get_data_converter_from_chars
        procedure :: build_data_converter_from_string => build_data_converter_from_string
        procedure :: build_data_converter_from_chars => build_data_converter_from_chars
        procedure :: get_host_converter_from_string => get_host_converter_from_string
        procedure :: get_host_converter_from_chars => get_host_converter_from_chars
        procedure :: build_host_converter_from_string => build_host_converter_from_string
        procedure :: build_host_converter_from_chars => build_host_converter_from_chars
        procedure :: allocate_host_converter => allocate_host_converter
        procedure :: build_buffered_converter => build_buffered_converter
    end type fortran_converter_factory

    character(len=*), parameter :: converter_driver_key = "converter_driver"
    character(len=*), parameter :: default_converter_driver = "regular"
contains
    subroutine build_data_converter_from_string(this, converter, key, options, priorities)
        class(fortran_converter_factory), intent(in) :: this
        type(fortran_data_converter), intent(inout) :: converter
        type(string), intent(in), optional :: key
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        call this%build_host_converter(converter%converter, key, options, priorities)
    end subroutine build_data_converter_from_string

    type(fortran_data_converter) function get_data_converter_from_string(this, key)
        class(fortran_converter_factory), intent(in) :: this
        type(string), intent(in) :: key

        call this%build_data_converter(get_data_converter_from_string, key)
    end function get_data_converter_from_string

    type(fortran_data_converter) function get_data_converter_from_chars(this, key)
        class(fortran_converter_factory), intent(in) :: this
        character(len=*), intent(in) :: key

        call this%build_data_converter(get_data_converter_from_chars, string(key))
    end function get_data_converter_from_chars

    subroutine build_data_converter_from_chars(this, converter, key, options, priorities)
        class(fortran_converter_factory), intent(in) :: this
        type(fortran_data_converter), intent(inout) :: converter
        character(len=*), intent(in) :: key
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

            call this%build_data_converter(converter, string(key), options, priorities)
    end subroutine build_data_converter_from_chars

    subroutine allocate_host_converter(this, converter, key)
        class(fortran_converter_factory), intent(in) :: this
        class(fortran_host_converter), allocatable, intent(inout) :: converter
        type(string), intent(in) :: key

        select case (key%char_array)
        case ("regular")
            allocate(fortran_host_converter::converter)
        case ("buffered")
            allocate(fhc_with_buffer::converter)
        end select
    end subroutine allocate_host_converter

    subroutine build_host_converter_from_string(this, converter, key, options, priorities)
        class(fortran_converter_factory), intent(in) :: this
        class(fortran_host_converter), allocatable, intent(inout) :: converter
        type(string), intent(in), optional :: key
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        class(data_builder), allocatable :: builder
        type(dictionary_converter) :: conv
        type(string), dimension(:), allocatable :: local_priorities

        call this%allocate_host_converter(converter, &
                conv%to_string(converter_driver_key, options, priorities, default_converter_driver))
        local_priorities = add_prefix_to_priorities("converter-", priorities)

        if ( present(key) ) then
            call memory_factory%create_data_builder(builder, key)
        else if ( present(options) ) then
            call memory_factory%create_data_builder(builder, options, local_priorities)
        else
            error stop "fortran_converter_factory::build_host_converter_from_string:Incorrect arguments."
        end if

        select type(builder)
        class is (host_builder)
            converter%builder = builder
        class default
            error stop "fortran_converter_factory:build_host_converter_from_string:Not a valid builder."
        end select
        deallocate(builder)

        select type(converter)
        type is (fhc_with_buffer)
            call this%build_buffered_converter(converter, options, local_priorities)
        end select
    end subroutine build_host_converter_from_string

    subroutine build_buffered_converter(this, converter, options, priorities)
        class(fortran_converter_factory), intent(in) :: this
        type(fhc_with_buffer), intent(inout) :: converter
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(scratch_buffer_builder) :: builder

        call builder%create(converter%buffer, options=options, priorities=priorities)
        call converter%buffer%initialize()
    end subroutine build_buffered_converter

    function get_host_converter_from_string(this, key) result(converter)
        class(fortran_converter_factory), intent(in) :: this
        type(string), intent(in) :: key
        class(fortran_host_converter), allocatable :: converter

        call this%build_host_converter(converter, key)
    end function get_host_converter_from_string

    function get_host_converter_from_chars(this, key) result(converter)
        class(fortran_converter_factory), intent(in) :: this
        character(len=*), intent(in) :: key
        class(fortran_host_converter), allocatable :: converter

        call this%build_host_converter(converter, string(key))
    end function get_host_converter_from_chars

    subroutine build_host_converter_from_chars(this, converter, key, options, priorities)
        class(fortran_converter_factory), intent(in) :: this
        class(fortran_host_converter), allocatable, intent(inout) :: converter
        character(len=*), intent(in) :: key
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        call this%build_host_converter(converter, string(key), options, priorities)
    end subroutine build_host_converter_from_chars
end module fortran_converter_factory_module
