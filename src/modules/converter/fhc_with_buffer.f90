module fhc_with_buffer_module
    use, intrinsic :: iso_fortran_env, only : int64

    use :: memory_api, only : data_storage
    use :: concurrency_api, only : &
            stream
    use :: fortran_host_converter_module, only : fortran_host_converter
    use :: scratch_buffer_module, only : scratch_buffer
    use :: host_storage_module, only : host_storage

    implicit none
    private

    public :: fhc_with_buffer

    type, extends(fortran_host_converter) :: fhc_with_buffer
        type(scratch_buffer) :: buffer
    contains
        procedure :: allocate_host_storage => allocate_host_storage
        procedure :: release_host_storage => release_host_storage
        procedure :: checkpoint => checkpoint
        procedure :: cleanup => cleanup
    end type fhc_with_buffer
contains
    subroutine allocate_host_storage(this, storage, number_of_bytes)
        class(fhc_with_buffer), intent(inout) :: this
        class(host_storage), allocatable, intent(inout) :: storage
        integer(int64), intent(in) :: number_of_bytes

        call this%builder%allocate_host_storage(storage)
        call this%buffer%create(storage, number_of_bytes, .false.)
    end subroutine allocate_host_storage

    subroutine release_host_storage(this, remote, local)
        class(fhc_with_buffer), intent(inout) :: this
        class(data_storage), intent(in) :: remote
        class(host_storage), intent(inout) :: local

        call this%buffer%destroy(local)
    end subroutine release_host_storage

    subroutine checkpoint(this, astream)
        class(fhc_with_buffer), intent(inout) :: this
        type(stream), intent(in), optional :: astream

        call this%buffer%checkpoint(astream)
    end subroutine checkpoint

    subroutine cleanup(this)
        class(fhc_with_buffer), intent(inout) :: this

        call this%buffer%cleanup()
        call this%fortran_host_converter%cleanup()
    end subroutine cleanup
end module fhc_with_buffer_module
