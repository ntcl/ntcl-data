module resource_pool_builder_module
    use, intrinsic :: iso_fortran_env, only : int64

    use :: util_api, only : &
            string, &
            dictionary, &
            dictionary_converter, &
            add_prefix_to_priorities

    use :: resource_pool_module, only : &
            resource_pool

    use :: scratch_buffer_api, only : &
            scratch_buffer, &
            default_scratch_buffer_builder

    use :: event_pool_module, only : &
            event_pool

    use :: stream_pool_module, only : &
            stream_pool

    implicit none
    private

    public :: resource_pool_builder
    public :: get_resource_pool
    public :: build_resource_pool

    type :: resource_pool_builder
    contains
        procedure :: get => get
        procedure :: build => build
        procedure :: cleanup => cleanup
        procedure, private :: build_scratch_buffer => build_scratch_buffer
        procedure, private :: build_stream_pool => build_stream_pool
        procedure, private :: get_number_of_streams => get_number_of_streams
        procedure, private :: build_event_pool => build_event_pool
        procedure, private :: get_number_of_events => get_number_of_events
        procedure, private :: clear => clear
    end type resource_pool_builder

    interface resource_pool_builder
        module procedure constructor
    end interface resource_pool_builder

    character(len=*), parameter :: resource_pool_prefix = "resource_pool-"
    character(len=*), parameter :: stream_pool_prefix = "stream_pool-"
    character(len=*), parameter :: event_pool_prefix = "event_pool-"
    character(len=*), parameter :: number_of_streams_key = "number_of_streams"
    character(len=*), parameter :: number_of_events_key = "number_of_events"
    integer, parameter :: default_number_of_streams = 20
    integer, parameter :: default_number_of_events = 20
contains
    type(resource_pool) function get_resource_pool(buffer_type, buffer_size, number_of_checkpoints, &
            number_of_slots, number_of_streams, number_of_events, options, priorities) result(pool)
        character(len=*), intent(in), optional :: buffer_type
        integer(int64), intent(in), optional :: buffer_size
        integer, intent(in), optional :: number_of_checkpoints, number_of_slots, &
                number_of_streams, number_of_events
        type(dictionary), intent(in), optional:: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(resource_pool_builder) :: builder

        pool = builder%get(buffer_type, buffer_size, number_of_checkpoints, &
                number_of_slots, number_of_streams, number_of_events, options, priorities)
    end function get_resource_pool

    subroutine build_resource_pool(pool, buffer_type, buffer_size, number_of_checkpoints, &
            number_of_slots, number_of_streams, number_of_events, options, priorities)
        type(resource_pool), intent(inout) :: pool
        character(len=*), intent(in), optional :: buffer_type
        integer(int64), intent(in), optional :: buffer_size
        integer, intent(in), optional :: number_of_checkpoints, number_of_slots, &
                number_of_streams, number_of_events
        type(dictionary), intent(in), optional:: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(resource_pool_builder) :: builder

        call builder%build(pool, buffer_type, buffer_size, number_of_checkpoints, &
                number_of_slots, number_of_streams, number_of_events, options, priorities)
    end subroutine build_resource_pool

    function constructor() result(this)
        type(resource_pool_builder) :: this

        call this%clear()
    end function constructor

    type(resource_pool) function get(this, buffer_type, buffer_size, number_of_checkpoints, &
            number_of_slots, number_of_streams, number_of_events, options, priorities) result(pool)
        class(resource_pool_builder), intent(in) :: this
        character(len=*), intent(in), optional :: buffer_type
        integer(int64), intent(in), optional :: buffer_size
        integer, intent(in), optional :: number_of_checkpoints, number_of_slots, &
                number_of_streams, number_of_events
        type(dictionary), intent(in), optional:: options
        type(string), dimension(:), intent(in), optional :: priorities

        call this%build(pool, buffer_type, buffer_size, number_of_checkpoints, &
                number_of_slots, number_of_streams, number_of_events, options, priorities)
    end function get

    subroutine build(this, pool, buffer_type, buffer_size, number_of_checkpoints, &
            number_of_slots, number_of_streams, number_of_events, options, priorities)
        class(resource_pool_builder), intent(in) :: this
        type(resource_pool), intent(inout) :: pool
        character(len=*), intent(in), optional :: buffer_type
        integer(int64), intent(in), optional :: buffer_size
        integer, intent(in), optional :: number_of_checkpoints, number_of_slots, &
                number_of_streams, number_of_events
        type(dictionary), intent(in), optional:: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(string), dimension(:), allocatable :: local_priorities

        call pool%cleanup()

        local_priorities = add_prefix_to_priorities(resource_pool_prefix, priorities)

        call this%build_scratch_buffer(pool%scratch, buffer_type, buffer_size, &
                number_of_checkpoints, number_of_slots, options, local_priorities)
        call this%build_stream_pool(pool%streams, number_of_streams, options, local_priorities)
        call this%build_event_pool(pool%events, number_of_events, options, local_priorities)
    end subroutine build

    subroutine build_scratch_buffer(this, scratch, buffer_type, buffer_size, &
            number_of_checkpoints, number_of_slots, options, priorities)
        class(resource_pool_builder), intent(in) :: this
        type(scratch_buffer), intent(inout) :: scratch
        character(len=*), intent(in), optional :: buffer_type
        integer(int64), intent(in), optional :: buffer_size
        integer, intent(in), optional :: number_of_checkpoints, number_of_slots
        type(dictionary), intent(in), optional:: options
        type(string), dimension(:), intent(in), optional :: priorities

        call default_scratch_buffer_builder%build(scratch, buffer_type, buffer_size, &
                number_of_checkpoints, number_of_slots, options, priorities)
    end subroutine build_scratch_buffer

    subroutine build_stream_pool(this, pool, number_of_streams, options, priorities)
        class(resource_pool_builder), intent(in) :: this
        type(stream_pool), intent(inout) :: pool
        integer, intent(in), optional :: number_of_streams
        type(dictionary), intent(in), optional:: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(string), dimension(:), allocatable :: local_priorities
        integer :: actual_number_of_streams

        local_priorities = add_prefix_to_priorities(stream_pool_prefix, priorities)
        actual_number_of_streams = this%get_number_of_streams( &
                number_of_streams, options, priorities)

        pool = stream_pool(actual_number_of_streams)
    end subroutine build_stream_pool

    integer function get_number_of_streams(this, &
            number_of_streams, options, priorities) result(pool_size)
        class(resource_pool_builder), intent(in) :: this
        integer, intent(in), optional :: number_of_streams
        type(dictionary), intent(in), optional:: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(dictionary_converter) :: converter

        if ( present(number_of_streams) ) then
            pool_size = number_of_streams
            return
        end if

        pool_size = converter%to_int( &
                number_of_streams_key, &
                options, &
                priorities, &
                default_number_of_streams )
    end function get_number_of_streams

    subroutine build_event_pool(this, pool, number_of_events, options, priorities)
        class(resource_pool_builder), intent(in) :: this
        type(event_pool), intent(inout) :: pool
        integer, intent(in), optional :: number_of_events
        type(dictionary), intent(in), optional:: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(string), dimension(:), allocatable :: local_priorities
        integer :: actual_number_of_events

        local_priorities = add_prefix_to_priorities(event_pool_prefix, priorities)
        actual_number_of_events = this%get_number_of_events( &
                number_of_events, options, priorities)

        pool = event_pool(actual_number_of_events)
    end subroutine build_event_pool

    integer function get_number_of_events(this, &
            number_of_events, options, priorities) result(pool_size)
        class(resource_pool_builder), intent(in) :: this
        integer, intent(in), optional :: number_of_events
        type(dictionary), intent(in), optional:: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(dictionary_converter) :: converter

        if ( present(number_of_events) ) then
            pool_size = number_of_events
            return
        end if

        pool_size = converter%to_int( &
                number_of_events_key, &
                options, &
                priorities, &
                default_number_of_events )
    end function get_number_of_events

    subroutine cleanup(this)
        class(resource_pool_builder), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(resource_pool_builder), intent(inout) :: this
    end subroutine clear
end module resource_pool_builder_module
