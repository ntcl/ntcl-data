module event_pool_module
    use :: util_api, only : &
            small_ring_counter

    use :: concurrency_api, only : &
            dependency_manager, &
            event

    implicit none
    private

    public :: event_pool

    type :: event_pool
        type(small_ring_counter) :: counter
        type(event), dimension(:), allocatable :: events
        logical :: initialized
    contains
        procedure :: get_event => get_event
        procedure :: is_event_available => is_event_available
        procedure :: get_next_event => get_next_event
        procedure :: get_next_available_event => get_next_available_event
        procedure :: wait_for_available_event => wait_for_available_event
        procedure :: cleanup => cleanup

        procedure, private :: initialize_events => initialize_events
        procedure, private :: clear => clear
    end type event_pool

    interface event_pool
        module procedure constructor_empty
        module procedure constructor
    end interface event_pool

contains
    function constructor_empty() result(this)
        type(event_pool) :: this

        call this%clear()
    end function constructor_empty

    function constructor(pool_size) result(this)
        integer, intent(in) :: pool_size
        type(event_pool) :: this

        this = event_pool()

        this%counter = small_ring_counter(pool_size)
        call this%initialize_events(pool_size)
    end function constructor

    type(event) function get_event(this, id) result(e)
        class(event_pool), intent(in) :: this
        integer, intent(in) :: id

        e = event()
        if ( this%initialized ) e = this%events(id)
    end function get_event

    logical function is_event_available(this, id)
        class(event_pool), intent(in) :: this
        integer, intent(in) :: id

        is_event_available = .false.
        if ( .not. this%initialized ) return

        is_event_available = dependency_manager%has_dependency_completed(this%events(id))
    end function is_event_available

    type(event) function get_next_event(this) result(e)
        class(event_pool), intent(inout) :: this

        e = event()
        if ( this%initialized ) &
                e = this%events( &
                        this%counter%get_next_and_increase())
    end function get_next_event

    type(event) function get_next_available_event(this) result(e)
        class(event_pool), intent(inout) :: this

        call this%wait_for_available_event()
        e = this%get_next_event()
    end function get_next_available_event

    subroutine wait_for_available_event(this, id)
        class(event_pool), intent(in) :: this
        integer, intent(in), optional :: id

        integer :: idx

        if ( .not. this%initialized ) return

        idx = this%counter%get_next()
        if ( present(id) ) idx = id

        if ( .not. this%is_event_available(idx) ) &
                call dependency_manager%synchronize(this%events(idx))
    end subroutine wait_for_available_event

    subroutine cleanup(this)
        class(event_pool), intent(inout) :: this

        integer :: idx

        if ( allocated(this%events) ) then
            do idx = 1, size(this%events)
                call dependency_manager%destroy_event(this%events(idx))
            end do
            deallocate(this%events)
        end if

        call this%clear()
    end subroutine cleanup

    subroutine initialize_events(this, pool_size)
        class(event_pool), intent(inout) :: this
        integer, intent(in) :: pool_size

        integer :: idx

        if ( pool_size < 1 ) &
                error stop 'event_pool::initialize_events:Non-positive pool size.'

        allocate(this%events(pool_size))
        do idx = 1, pool_size
            this%events(idx) = dependency_manager%get_new_event()
        end do
        this%initialized = .true.
    end subroutine initialize_events

    subroutine clear(this)
        class(event_pool), intent(inout) :: this

        this%initialized = .false.
        this%counter = small_ring_counter()
    end subroutine clear
end module event_pool_module
