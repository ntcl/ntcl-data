module resource_pool_api
    use :: event_pool_module
    use :: stream_pool_module
    use :: scratch_buffer_api
    use :: resource_pool_module
    use :: resource_pool_builder_module

    implicit none
    public
end module resource_pool_api
