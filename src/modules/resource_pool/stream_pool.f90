module stream_pool_module
    use :: util_api, only : &
            small_ring_counter

    use :: concurrency_api, only : &
            dependency_manager, &
            stream

    implicit none
    private

    public :: stream_pool

    type :: stream_pool
        type(small_ring_counter) :: counter
        type(stream), dimension(:), allocatable :: streams
        logical :: initialized
    contains
        procedure :: get_next_stream => get_next_stream
        procedure :: cleanup => cleanup

        procedure, private :: initialize_streams => initialize_streams
        procedure, private :: clear => clear
    end type stream_pool

    interface stream_pool
        module procedure constructor_empty
        module procedure constructor
    end interface stream_pool

contains
    function constructor_empty() result(this)
        type(stream_pool) :: this

        call this%clear()
    end function constructor_empty

    function constructor(pool_size) result(this)
        integer, intent(in) :: pool_size
        type(stream_pool) :: this

        this = stream_pool()

        this%counter = small_ring_counter(pool_size)
        call this%initialize_streams(pool_size)
    end function constructor

    type(stream) function get_next_stream(this) result(s)
        class(stream_pool), intent(inout) :: this

        s = stream()
        if ( this%initialized ) &
                s = this%streams( &
                        this%counter%get_next_and_increase())
    end function get_next_stream

    subroutine cleanup(this)
        class(stream_pool), intent(inout) :: this

        integer :: idx

        if ( allocated(this%streams) ) then
            do idx = 1, size(this%streams)
                call dependency_manager%destroy_stream(this%streams(idx))
            end do
            deallocate(this%streams)
        end if

        call this%clear()
    end subroutine cleanup

    subroutine initialize_streams(this, pool_size)
        class(stream_pool), intent(inout) :: this
        integer, intent(in) :: pool_size

        integer :: idx

        if ( pool_size < 1 ) &
                error stop 'stream_pool::initialize_streams:Non-positive pool size.'

        allocate(this%streams(pool_size))
        do idx = 1, pool_size
            this%streams(idx) = dependency_manager%get_new_stream()
        end do
        this%initialized = .true.
    end subroutine initialize_streams

    subroutine clear(this)
        class(stream_pool), intent(inout) :: this

        this%initialized = .false.
        this%counter = small_ring_counter()
    end subroutine clear
end module stream_pool_module
