module checkpoint_manager_module
    use, intrinsic :: iso_fortran_env, only : int64
    use, intrinsic :: iso_c_binding, only : c_ptr

    use :: concurrency_api, only : &
            event, &
            stream, &
            dependency_manager

    use :: ring_array_descriptor_module, only : ring_array_descriptor
    use :: event_pool_module, only : event_pool

    implicit none
    private

    public :: checkpoint_manager

    type :: checkpoint_manager
        type(event_pool) :: checkpoints
        type(ring_array_descriptor) :: counter
    contains
        procedure :: initialize => initialize
        procedure :: register_checkpoint => register_checkpoint
        procedure :: process_next_checkpoint => process_next_checkpoint
        procedure :: reset => reset
        procedure :: has_room => has_room
        procedure :: cleanup => cleanup
        procedure :: wait_for_next_checkpoint => wait_for_next_checkpoint
        procedure :: wait_for_checkpoint => wait_for_checkpoint
        procedure :: wait_for_all_checkpoints => wait_for_all_checkpoints
        procedure :: clear => clear
    end type checkpoint_manager

    interface checkpoint_manager
        module procedure constructor_empty
        module procedure constructor
    end interface checkpoint_manager
contains
    function constructor_empty() result(this)
        type(checkpoint_manager) :: this

        call this%clear()
    end function constructor_empty

    function constructor(number_of_checkpoints) result(this)
        integer, intent(in) :: number_of_checkpoints
        type(checkpoint_manager) :: this

        this = checkpoint_manager()
        this%counter = ring_array_descriptor(number_of_checkpoints)
    end function constructor

    subroutine initialize(this)
        class(checkpoint_manager), intent(inout) :: this

        this%checkpoints = event_pool(this%counter%array_size)
        call this%reset()
    end subroutine initialize

    integer function register_checkpoint(this, astream)
        class(checkpoint_manager), intent(inout) :: this
        type(stream), intent(in), optional :: astream

        type(event) :: e

        register_checkpoint = this%counter%get_next_index()
        e = this%checkpoints%get_event(register_checkpoint)
        call this%counter%increase()
        call dependency_manager%register_dependency( e, astream)
    end function register_checkpoint

    logical function has_room(this)
        class(checkpoint_manager), intent(in) :: this

        has_room = .not. this%counter%is_full()
    end function has_room

    integer function process_next_checkpoint(this)
        class(checkpoint_manager), intent(inout) :: this

        integer :: chk

        process_next_checkpoint = 0
        if ( this%counter%elements_in_use == 0 ) return

        call this%wait_for_next_checkpoint()

        chk = this%counter%get_array_index(1)
        if ( this%checkpoints%is_event_available(chk) ) then
            call this%counter%decrease()
            process_next_checkpoint = chk
        end if
    end function process_next_checkpoint

    subroutine wait_for_all_checkpoints(this)
        class(checkpoint_manager), intent(inout) :: this

        integer :: idx

        do idx = 1, this%counter%elements_in_use
            call this%wait_for_checkpoint(this%counter%get_array_index(idx))
        end do
    end subroutine wait_for_all_checkpoints

    subroutine wait_for_next_checkpoint(this)
        class(checkpoint_manager), intent(inout) :: this


        call this%wait_for_checkpoint(this%counter%get_next_index())
    end subroutine wait_for_next_checkpoint

    subroutine wait_for_checkpoint(this, chk)
        class(checkpoint_manager), intent(inout) :: this
        integer, intent(in) :: chk

        call this%checkpoints%wait_for_available_event(chk)
    end subroutine wait_for_checkpoint

    subroutine reset(this)
        class(checkpoint_manager), intent(inout) :: this

        call this%wait_for_all_checkpoints()
        call this%counter%reset()
    end subroutine reset

    subroutine cleanup(this)
        class(checkpoint_manager), intent(inout) :: this

        integer :: idx

        call this%wait_for_all_checkpoints()

        call this%counter%cleanup()
        call this%checkpoints%cleanup()

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(checkpoint_manager), intent(inout) :: this

        this%counter = ring_array_descriptor()
        this%checkpoints = event_pool()
    end subroutine clear
end module checkpoint_manager_module
