module resource_pool_module
    use :: event_pool_module, only : event_pool
    use :: stream_pool_module, only : stream_pool
    use :: scratch_buffer_module, only : scratch_buffer

    implicit none
    private

    public :: resource_pool

    type :: resource_pool
        type(event_pool) :: events
        type(stream_pool) :: streams
        type(scratch_buffer) :: scratch
    contains
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type resource_pool

    interface resource_pool
        module procedure constructor_empty
        module procedure constructor
    end interface resource_pool

contains
    function constructor_empty() result(this)
        type(resource_pool) :: this

        call this%clear()
    end function constructor_empty

    function constructor(events, streams, scratch) result(this)
        type(event_pool), intent(in) :: events
        type(stream_pool), intent(in) :: streams
        type(scratch_buffer), intent(in) :: scratch
        type(resource_pool) :: this

        this = resource_pool()
        this%events = events
        this%streams = streams
        this%scratch = scratch
    end function constructor

    subroutine cleanup(this)
        class(resource_pool), intent(inout) :: this

        call this%scratch%cleanup()
        call this%streams%cleanup()
        call this%events%cleanup()
        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(resource_pool), intent(inout) :: this

        this%events = event_pool()
        this%streams = stream_pool()
        this%scratch = scratch_buffer()
    end subroutine clear
end module resource_pool_module
