module scratch_buffer_builder_module
    use, intrinsic :: iso_fortran_env, only : int64

    use :: util_api, only : &
            string, &
            dictionary, &
            options_handler, &
            add_prefix_to_priorities

    use :: memory_api, only : &
            data_storage, &
            data_builder, &
            memory_factory
    use :: concurrency_api, only : &
            concurrency_factory

    use :: scratch_buffer_module, only : scratch_buffer
    use :: ring_buffer_module, only : ring_buffer
    use :: slot_manager_module, only : slot_manager
    use :: checkpoint_manager_module, only : checkpoint_manager

    implicit none
    private

    public :: scratch_buffer_builder

    type :: scratch_buffer_builder
        type(dictionary) :: default_options
    contains
        procedure :: set_default_options => set_default_options
        procedure :: create => create
        procedure :: get => get
        procedure :: build => build
        procedure :: cleanup => cleanup
        procedure, private :: build_ring_buffer => build_ring_buffer
        procedure, private :: build_checkpoint_manager => build_checkpoint_manager
        procedure, private :: build_slot_manager => build_slot_manager
    end type scratch_buffer_builder

    character(len=*), parameter :: prefix = "scratch_buffer-"
    character(len=*), parameter :: buffer_size_key = "size"
    character(len=*), parameter :: number_of_checkpoints_key = "number_of_checkpoints"
    character(len=*), parameter :: number_of_slots_key = "number_of_slots"
    integer(int64), parameter :: default_buffer_size = 10*1024*1024
    integer, parameter :: default_number_of_slots = 50
    integer, parameter :: default_number_of_checkpoints = 10
contains
    subroutine set_default_options(this, options)
        class(scratch_buffer_builder), intent(inout) :: this
        type(dictionary), intent(in) :: options

        this%default_options = options
    end subroutine set_default_options

    function get(this, memory_type, options, priorities) result(scratch)
        class(scratch_buffer_builder), intent(in) :: this
        character(len=*), intent(in), optional :: memory_type
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        type(scratch_buffer) :: scratch

        call this%create(scratch, memory_type, options, priorities)
    end function get

    subroutine build(this, scratch, buffer_type, buffer_size, &
            number_of_checkpoints, number_of_slots, options, priorities)
        class(scratch_buffer_builder), intent(in) :: this
        type(scratch_buffer), intent(inout) :: scratch
        character(len=*), intent(in), optional :: buffer_type
        integer(int64), intent(in), optional :: buffer_size
        integer, intent(in), optional :: number_of_checkpoints, number_of_slots
        type(dictionary), intent(in), optional:: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(string), dimension(:), allocatable :: local_priorities

        local_priorities = add_prefix_to_priorities(prefix, priorities)

        scratch = scratch_buffer()
        call this%build_ring_buffer(scratch%buffer, buffer_type, buffer_size, &
                options, local_priorities)
        call this%build_checkpoint_manager(scratch%checkpoints, number_of_checkpoints, &
                options, local_priorities)
        call this%build_slot_manager(scratch%slots, number_of_slots, &
                options, local_priorities)
    end subroutine build

    subroutine create(this, scratch, memory_type, options, priorities)
        class(scratch_buffer_builder), intent(in) :: this
        type(scratch_buffer), intent(inout) :: scratch
        character(len=*), intent(in), optional :: memory_type
        type(dictionary), intent(in), optional:: options
        type(string), dimension(:), intent(in), optional :: priorities

        call this%build(scratch, memory_type, options=options, priorities=priorities)
    end subroutine create

    subroutine build_ring_buffer(this, buffer, buffer_type, buffer_size, &
            options, priorities)
        class(scratch_buffer_builder), intent(in) :: this
        type(ring_buffer), intent(inout) :: buffer
        character(len=*), intent(in), optional :: buffer_type
        integer(int64), intent(in), optional :: buffer_size
        type(dictionary), intent(in), optional:: options
        type(string), dimension(:), intent(in), optional :: priorities
        
        class(data_builder), allocatable :: builder
        class(data_storage), allocatable :: storage
        type(options_handler) :: handler

        buffer = ring_buffer( &
                handler%to_int64(buffer_size, buffer_size_key, options, &
                        this%default_options, priorities, default_buffer_size) )
        call memory_factory%create_default_data_builder(buffer%builder, &
                buffer_type, options, priorities)
        call buffer%builder%allocate_local_storage(buffer%internal_buffer)
    end subroutine build_ring_buffer

    subroutine build_checkpoint_manager(this, manager, number_of_checkpoints, options, priorities)
        class(scratch_buffer_builder), intent(in) :: this
        type(checkpoint_manager), intent(inout) :: manager
        integer, intent(in), optional :: number_of_checkpoints
        type(dictionary), intent(in), optional:: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(options_handler) :: handler

        manager = checkpoint_manager( &
                handler%to_int(number_of_checkpoints, number_of_checkpoints_key, options, &
                        this%default_options, priorities, default_number_of_checkpoints) )
    end subroutine build_checkpoint_manager

    subroutine build_slot_manager(this, manager, number_of_slots, options, priorities)
        class(scratch_buffer_builder), intent(in) :: this
        type(slot_manager), intent(inout) :: manager
        integer, intent(in), optional :: number_of_slots
        type(dictionary), intent(in), optional:: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(options_handler) :: handler

        manager = slot_manager( &
                handler%to_int(number_of_slots, number_of_slots_key, options, &
                        this%default_options, priorities, default_number_of_slots) )
    end subroutine build_slot_manager

    subroutine cleanup(this)
        class(scratch_buffer_builder), intent(inout) :: this

        call this%default_options%cleanup()
    end subroutine cleanup
end module scratch_buffer_builder_module
