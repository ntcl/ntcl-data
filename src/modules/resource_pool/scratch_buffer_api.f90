module scratch_buffer_api
    use, intrinsic :: iso_fortran_env, only : int64

    use :: util_api, only : &
            string, &
            dictionary, &
            dictionary_converter, &
            string_converter
    use :: scratch_buffer_module, only : scratch_buffer
    use :: scratch_buffer_builder_module, only : scratch_buffer_builder

    implicit none
    private

    public :: scratch_buffer
    public :: scratch_buffer_builder
    public :: create_scratch_buffer
    public :: get_scratch_buffer
    public :: global_scratch_buffer
    public :: default_scratch_buffer_builder

    public :: scratch_buffer_initialize
    public :: scratch_buffer_finalize
    public :: setup_global_scratch_buffer

    type(scratch_buffer) :: global_scratch_buffer
    type(scratch_buffer_builder) :: default_scratch_buffer_builder
contains
    type(scratch_buffer) function get_scratch_buffer(memory_type, options, priorities)
        character(len=*), intent(in), optional :: memory_type
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        get_scratch_buffer = default_scratch_buffer_builder%get(memory_type, options, priorities)
    end function get_scratch_buffer

    subroutine create_scratch_buffer(scratch, memory_type, options, priorities)
        type(scratch_buffer), intent(inout) :: scratch
        character(len=*), intent(in), optional :: memory_type
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        call default_scratch_buffer_builder%create(scratch, memory_type, options, priorities)
    end subroutine create_scratch_buffer

    subroutine scratch_buffer_initialize(options)
        type(dictionary), intent(in), optional :: options

        if ( present(options) ) &
                call default_scratch_buffer_builder%set_default_options(options)

        global_scratch_buffer = scratch_buffer()
        if ( enable_global_scratch_buffer(options) ) &
                call setup_global_scratch_buffer(options)
    end subroutine scratch_buffer_initialize

    subroutine scratch_buffer_finalize(options)
        type(dictionary), intent(in), optional :: options

        call global_scratch_buffer%cleanup()
        call default_scratch_buffer_builder%cleanup()
    end subroutine scratch_buffer_finalize

    subroutine setup_global_scratch_buffer(options)
        type(dictionary), intent(in), optional :: options

        type(dictionary) :: local_options
        integer(int64) :: scratch_buffer_size
        type(string_converter) :: conv

        if ( present(options) ) then
            local_options = options
        else
            local_options = dictionary()
        end if


#if defined GLOBAL_SCRATCH_BUFFER_SIZE
        scratch_buffer_size = GLOBAL_SCRATCH_BUFFER_SIZE*1000000
        if ( .not. local_options%has_key('global_scratch_buffer-size') ) &
            call local_options%set_value(string('global_scratch_buffer-size'), &
                    conv%from_int64(scratch_buffer_size))
#endif
        call create_scratch_buffer( &
                global_scratch_buffer, &
                options=local_options, &
                priorities=[string("global_scratch_buffer-")])
        call global_scratch_buffer%initialize()
    end subroutine setup_global_scratch_buffer

    logical function enable_global_scratch_buffer(options) result(res)
        type(dictionary), intent(in), optional :: options

        type(dictionary_converter) :: conv

        res = .false.
#if defined ENABLE_GLOBAL_SCRATCH_BUFFER
        res = .true.
#endif

        res = conv%to_logical('enable_global_scratch_buffer', &
                options, default_value=res)
    end function enable_global_scratch_buffer
end module scratch_buffer_api
