module data_initializer
    use :: util_api, only : &
            dictionary

    use :: data_dev, only : &
            memory_initialize, &
            memory_finalize, &
            concurrency_initialize, &
            concurrency_finalize, &
            scratch_buffer_initialize, &
            scratch_buffer_finalize, &
            default_memory_factory, &
            default_concurrency_factory

    implicit none
    private

    public :: data_initialize
    public :: data_finalize
contains
    subroutine data_initialize(options)
        type(dictionary), intent(in), optional :: options

        call memory_initialize(default_memory_factory(), options)
        call concurrency_initialize(default_concurrency_factory(), options)
        call scratch_buffer_initialize(options)
    end subroutine data_initialize

    subroutine data_finalize()
        call scratch_buffer_finalize()
        call concurrency_finalize()
        call memory_finalize()
    end subroutine data_finalize
end module data_initializer
