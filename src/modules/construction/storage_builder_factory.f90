module storage_builder_factory_module
    use :: util_api, only : &
        string, &
        dictionary

    use :: memory_api, only : &
            memory_factory, &
            data_storage

    use :: storage_builder_module, only : storage_builder
    use :: data_builder_module, only : data_builder

    implicit none
    private

    public :: create_storage_builder
    public :: get_storage_builder
    public :: create_storage_builder_from_storage

contains
    function get_storage_builder(memory_type, options, priorities) result(builder)
        character(len=*), intent(in), optional :: memory_type
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        type(storage_builder) :: builder

        call create_storage_builder(builder, memory_type, options, priorities)
    end function get_storage_builder

    subroutine create_storage_builder(builder, memory_type, options, priorities)
        type(storage_builder), intent(inout) :: builder
        character(len=*), intent(in), optional :: memory_type
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        class(data_builder), allocatable :: dbuilder

        call memory_factory%create_default_data_builder(dbuilder, memory_type, options, priorities)
        builder = storage_builder(dbuilder)
        deallocate(dbuilder)
    end subroutine create_storage_builder

    subroutine create_storage_builder_from_storage(builder, storage)
        type(storage_builder), intent(inout) :: builder
        class(data_storage), intent(in) :: storage

        call memory_factory%create_data_builder(builder%builder, storage)
    end subroutine create_storage_builder_from_storage
end module storage_builder_factory_module
