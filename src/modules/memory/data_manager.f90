module data_manager_module
    use, intrinsic :: iso_fortran_env, only : &
            int64

    use :: util_api, only : &
            slice, &
            tile

    use :: stream_module, only : stream
    use :: data_storage_module, only : data_storage

    implicit none
    private

    public :: data_manager

    type, abstract :: data_manager
    contains
        procedure(copy_storage_interface), deferred :: copy_storage
        procedure(copy_2d_interface), deferred :: copy_2d
        procedure(copy_storage_interface), deferred :: secure_local_storage
        procedure(copy_storage_interface), deferred :: secure_local_storage_wo_copy
        procedure(copy_storage_interface), deferred :: update_remote_storage
        procedure(copy_storage_interface), deferred :: release_local_storage
        procedure :: update_remote_and_release_local => update_remote_and_release_local
        procedure :: copy_slice => copy_slice
    end type data_manager

    abstract interface
        subroutine copy_storage_interface(this, src, dst, astream)
            import :: data_manager
            import :: data_storage
            import :: stream

            class(data_manager), intent(in) :: this
            class(data_storage), intent(in) :: src
            class(data_storage), intent(inout) :: dst
            type(stream), intent(in), optional :: astream
        end subroutine copy_storage_interface

        subroutine copy_2d_interface(this, dst, dst_dims, dst_tile, src, src_dims, src_tile, astream)
            import :: data_manager
            import :: data_storage
            import :: int64
            import :: tile
            import :: stream

            class(data_manager), intent(in) :: this
            class(data_storage), intent(inout) :: dst
            integer(int64), dimension(2), intent(in) :: dst_dims
            type(tile), intent(in) :: dst_tile
            class(data_storage), intent(in) :: src
            integer(int64), dimension(2), intent(in) :: src_dims
            type(tile), intent(in) :: src_tile
            type(stream), intent(in), optional :: astream
        end subroutine copy_2d_interface
    end interface
contains
    subroutine update_remote_and_release_local(this, local, remote, astream)
        class(data_manager), intent(in) :: this
        class(data_storage), intent(inout) :: local, remote
        type(stream), intent(in), optional :: astream

        call this%update_remote_storage(local, remote, astream)
        call this%release_local_storage(remote, local, astream)
    end subroutine update_remote_and_release_local

    subroutine copy_slice(this, dst, dst_slice, src, src_slice, astream)
        class(data_manager), intent(in) :: this
        class(data_storage), intent(inout) :: dst
        type(slice), intent(in) :: dst_slice
        class(data_storage), intent(in) :: src
        type(slice), intent(in) :: src_slice
        type(stream), intent(in), optional :: astream

        class(data_storage), allocatable :: dst_ptr, src_ptr

        allocate(dst_ptr, mold=dst)
        allocate(src_ptr, mold=src)

        call dst_ptr%set_data_pointer( &
                dst%get_pointer_offset(dst_slice%offset), &
                dst_slice%nsize)

        call src_ptr%set_data_pointer( &
                src%get_pointer_offset(src_slice%offset), &
                src_slice%nsize)

        call this%copy_storage(src_ptr, dst_ptr, astream)

        deallocate(dst_ptr, src_ptr)
    end subroutine copy_slice
end module data_manager_module
